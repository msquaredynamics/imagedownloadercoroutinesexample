package com.unox.imagedownloaderexample

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.unox.imagedownloaderexample.models.RemoteImage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.image_container.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import java.net.URL
import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.random.Random

val imagesUrls = listOf(
    "https://banner2.kisspng.com/20171202/e3e/fruit-free-png-image-5a22fd776d6943.7240975215122425514482.jpg",
    "http://www.pngmart.com/files/5/Red-Apple-PNG-Photos.png",
    "https://www.clipartmax.com/png/middle/66-661522_rip-banana-fruit-transparent-background-png-image-banana-png.png",
    "https://banner2.kisspng.com/20171127/d9f/fruit-basket-png-vector-clipart-image-5a1c01ee7dd394.8919843915117849425154.jpg"
)

var imagesList = MutableList(imagesUrls.size) {
    RemoteImage(imagesUrls[it])
}

class MainActivity : AppCompatActivity(), CoroutineScope {
    private lateinit var job: Job
    override lateinit var coroutineContext: CoroutineContext

    private lateinit var adapter: MyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        job = Job()
        coroutineContext = Dispatchers.Main + job


        adapter = MyAdapter(imagesList)
        val layoutManager = GridLayoutManager(this, 3)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager

        button_singletask_load.setOnClickListener { loadImages() }

        loadImages()

    }


    /** Initiates the download of the images */
    private fun loadImages() {
        button_singletask_load.isEnabled = false

        // Reset all images in the list
        imagesList.forEach {
            it.bitmapRes = null
            it.loadingError = false

        }

        adapter.notifyDataSetChanged()

        // Start the download of each image
        imagesUrls.forEachIndexed { index, url ->

            // Launch a coroutine in a thread in the background threads pool
            val bitmap = async(Dispatchers.IO) { ImageDownloadManager.downloadImage(url) }

            // Launch a coroutine that waits for the bitmapReq async call to finish. Since this activity implements
            // the CoroutineScope interface, we can use launch directly. The coroutine will run on the UI thread
            // because the coroutineContext is defined as "Dispatchers.Main + job" (see onResume() )
            launch {
                bitmap.await().also {
                    imagesList[index].apply {
                        bitmapRes = it
                        loadingError = it == null
                    }
                    adapter.notifyItemChanged(index)

                    if (!ImageDownloadManager.pendingDownloads())
                        button_singletask_load.isEnabled = true
                }
            }
        }
    }


    override fun onDestroy() {
        // Cancel all coroutines declared within the scope of the activity
        job.cancel()
        super.onDestroy()
    }

}


/**
 * Adapter class that simply renders an ImageView and a circular ProgressBar while a download is running.
 * During the download or when a download failed, an image placeholder is used instead of the actual picture.
 */
class MyAdapter(private val dataset: List<RemoteImage>) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val imageView = view.imageView
        val progress = view.progressBar
    }

    override fun getItemCount(): Int {
        return dataset.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (dataset[position].bitmapRes != null) {
            holder.imageView.setImageBitmap(dataset[position].bitmapRes)
            holder.progress.visibility = View.GONE
        } else {
            holder.progress.visibility = if (dataset[position].loadingError) View.GONE else View.VISIBLE
            holder.imageView.setImageResource(R.drawable.ic_image_black_24dp)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.image_container, parent, false)
        return ViewHolder(view)
    }
}




object ImageDownloadManager {
    // Mutex() is in the  coroutines library and represents a mutex for accessing variables from multiple threads.
    private val mutex = Mutex()
    private var activeDownloadsCounter = 0

    /**
     * Request the download of an image resource from a remote url
     * @param urlString String containing the URL of the remote image.
     *
     * @return The downloaded bitmap object or null if download failed
     */
    suspend fun downloadImage(urlString: String) : Bitmap? {
        // withLock locks the mutex, executes the code inside the lambda, and unlocks the mutex
        mutex.withLock { activeDownloadsCounter++ }
        try {
            delay(3000)

            // Simulate randomic download failures
            val shouldFail = Random.nextDouble(0.0, 2.0) < 0.5
            if (shouldFail) {
                mutex.withLock { activeDownloadsCounter-- }
                return null
            }

            URL(urlString).readBytes().apply {
                mutex.withLock { activeDownloadsCounter-- }
                return BitmapFactory.decodeByteArray(this, 0, this.size)
            }
        } catch(ex: CancellationException) {
            // If the user rotates the screen while we are waiting for the delay, the coroutines will be cancelled and
            // a CancellationException will be thrown. Here, we decrement the active download counter before returning
            mutex.withLock { activeDownloadsCounter-- }
            return null
        }
    }

    /**
     * Returns true if there are active downloads
     */
    fun pendingDownloads() = activeDownloadsCounter != 0

}



